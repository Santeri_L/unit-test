const expect = require('chai').expect;
const mylib = require("../src/mylib");

describe("Our first unit tests", () => {
    before(() => {
        // initialization
        // create objects... etc...
        console.log("Initialising tests") 
    });
    it("Can add 1 and 2 together", () => {
        expect(mylib.add(1,2)).equal(3, "1 + 2 is not 3, for some reason?");
    });
    it("Can subtract 5 with 3", () => {
        expect(mylib.subtract(5,3)).equal(2, "5 - 3 is not 2, for some reason?");
    });
    it("Can divide 6 with 3", () => {
        expect(mylib.divide(6,3)).equal(2, "6 / 3 is not 2, for some reason?");
    });
    it("Can multiply 6 with 3", () => {
        expect(mylib.multiply(6,3)).equal(18, "6 x 3 is not 18, for some reason?");
    });
    it("0 is invalid divisor", () => {
        expect(mylib.divide(8,0)).to.throw("Error: divisor is 0");
    });
    after("", () => {
        // Cleanup
        // For example: shutdown the Express server.
        console.log("Testing completed!")
    });
});

