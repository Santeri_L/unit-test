 

/**Basic arithmetic operations */
const myLib = {
    /**Multiline arrow function. */
    add: (a, b) => {
        const sum = a + b;
        return sum;
    },
    subtract: (a, b) => {
        return a - b;
    },
    /** Singleline arrow function. */
    //divide: (dividend, divisor) => dividend / divisor,
    divide: (dividend, divisor) =>{
        if (divisor == 0)
        {
            throw 'Error: divisor is 0';
        }
        else 
        {
            return dividend / divisor;
        }
    },
    /** Regular function */
    multiply: function(a, b) {
        return a * b;
    }
};

module.exports = myLib;